package org.gusmox;

/**
 * <b>Sorting App</b>
 * <p></p>
 * This app takes a String of ints from System.in and will sort them on ascending order
 * to be returned as a String through System.out.
 * <p></p>
 * <b>Limitations:</b>
 * <ul>
 *     <li>Only accepts ints arguments.</li>
 *     <li>Only accepts from 1 to 10 arguments.</li>
 * </ul>
 *
 * @author  Agustin
 * @version 1.0
 * @since   03/05/2022
 */
public class Main{
    /**
     * This is the main method which makes use of SortingApp().solve() method.
     *
     * @exception IllegalArgumentException On non int input argument.
     * @exception IllegalArgumentException On less than 1 input argument.
     * @exception IllegalArgumentException On more than 10 input arguments.
     */
    public static void main( String[] args ) throws IllegalArgumentException{
        new SortingApp().solve();
    }
}
