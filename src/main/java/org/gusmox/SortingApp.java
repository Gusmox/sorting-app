package org.gusmox;

import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.Scanner;

/**
 * Class for sorting the input arguments through System.in.
 */
public class SortingApp {
    /**
     * Method of the class to take the System.in arguments, sort them in ascending order and print through System.out the sorted arguments.
     */
    public void solve() {
        try {
            Scanner inn = new Scanner(System.in);
            // Create array of strings from the command-line.
            String[] in = inn.nextLine().split(" ");
            // Validate the number of values from the command-line.
            if (in.length < 11) {
                // Create array of int, sort them in ascending order and return.
                int[] num = new int[in.length];
                for (int i = 0; i < in.length; i++) {
                    num[i] = Integer.parseInt(in[i]);
                }
                Arrays.sort(num);
                String out = Arrays.toString(num).replaceAll(",\\s", " ");
                System.out.println(out.replaceAll("[\\[\\]\"]", ""));
                // Throw IllegalArgumentException due to number of values above 10.
            } else {
                throw new IllegalArgumentException("Only one to ten values are accepted.");
            }
            // Catch NumberFormatException and throw IllegalArgumentException.
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Only int are accepted.");
            // Catch NoSuchElementException and throw IllegalArgumentException.
        } catch (NoSuchElementException e) {
            throw new IllegalArgumentException("Empty input.");
        }
    }
}

