package org.gusmox;

import static org.junit.Assert.assertEquals;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class SortingAppTest {

    protected SortingApp sortingApp = new SortingApp();

    private final String input;
    private final String expected;

    public SortingAppTest(String input, String expected) {
        this.input = input;
        this.expected = expected;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {"1", "1"}, // Corner case with 1 argument.
                {"2 1", "1 2"},
                {"3 2 1", "1 2 3"},
                {"4 3 2 1", "1 2 3 4"},
                {"5 4 3 2 1", "1 2 3 4 5"},
                {"6 5 4 3 2 1", "1 2 3 4 5 6"},
                {"7 6 5 4 3 2 1", "1 2 3 4 5 6 7"},
                {"8 7 6 5 4 3 2 1", "1 2 3 4 5 6 7 8"},
                {"9 8 7 6 5 4 3 2 1", "1 2 3 4 5 6 7 8 9"},
                {"10 9 8 7 6 5 4 3 2 1", "1 2 3 4 5 6 7 8 9 10"}, // Corner case with 10 arguments.
        });
    }

    @Test
    public void checkMethod() {
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
        InputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);
        sortingApp.solve();
        String output = new String(outContent.toByteArray()).replaceAll("(\\r|\\n)", "");
        assertEquals(expected, output);
    }

}
