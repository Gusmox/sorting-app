package org.gusmox;

import static org.junit.Assert.assertEquals;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.PrintStream;

public class SortingAppExceptionTest {

    protected SortingApp sortingApp = new SortingApp();

    // Check case with no int argument.
    @Test(expected = IllegalArgumentException.class)
    public void checkWrongInput() {
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
        String input = "A 2 4 1";
        InputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);
        sortingApp.solve();
        String output = new String(outContent.toByteArray()).replaceAll("(\\r|\\n)", "");
        String expected = "1 2 4 8";
        assertEquals(expected, output);
    }

    // Corner case with more than 10 arguments.
    @Test(expected = IllegalArgumentException.class)
    public void checkNumberOfInput() {
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
        String input = "12 11 10 9 8 7 6 5 4 3 2 1";
        InputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);
        sortingApp.solve();
        String output = new String(outContent.toByteArray()).replaceAll("(\\r|\\n)", "");
        String expected = "1 2 3 4 5 6 7 8 9 10 11 12";
        assertEquals(expected, output);
    }

    // Corner case with zero argument.
    @Test(expected = IllegalArgumentException.class)
    public void checkEmptyInput() {
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
        String input = "";
        InputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);
        sortingApp.solve();
        String output = new String(outContent.toByteArray()).replaceAll("(\\r|\\n)", "");
        String expected = "";
        assertEquals(expected, output);
    }
}
